# Annuaire des acteurs et actrices de l'accompagnement au numérique libre - Flux

Exploration de l\'annuaire des acteurs et actrices de l'accompagnement
au numérique libre, notamment de leurs flux RSS

-   Free software: GNU General Public License v3
-   Documentation:
    <https://enquete-accompagnements-planete.readthedocs.io>.

Framasoft a publié un [Annuaire des acteurs et actrices de l’accompagnement
au numérique libre](https://framablog.org/2020/09/15/annuaire-des-acteurs-et-actrices-de-laccompagnement-au-numerique-libre/)

Voici un script python pour générer un fichier OPML à partir de cet annuaire,
encourageant ainsi l'usage de RSS et d'un web décentralisé.

## Projet d'origine

https://framagit.org/derivation/enquete-accompagnements

## Credits

This package was created with
[Cookiecutter](https://github.com/audreyr/cookiecutter) and the
[audreyr/cookiecutter-pypackage](https://github.com/audreyr/cookiecutter-pypackage)
project template.

Image de couverture par David Revoy sous licence CC BY. https://www.davidrevoy.com/article626/contributopia
