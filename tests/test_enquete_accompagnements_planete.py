#!/usr/bin/env python

"""Tests for `enquete_accompagnements_planete` package."""

# from click.testing import CliRunner

from enquete_accompagnements_planete import enquete_accompagnements_planete
# from enquete_accompagnements_planete import cli


def test_process():
    """Sample pytest test function with the pytest fixture as an argument."""
    enquete_accompagnements_planete.process()


# def test_command_line_interface():
#     """Test the CLI."""
#     runner = CliRunner()
#     result = runner.invoke(cli.main)
#     assert result.exit_code == 0
#     assert 'enquete_accompagnements_planete.cli.main' in result.output
#     help_result = runner.invoke(cli.main, ['--help'])
#     assert help_result.exit_code == 0
#     assert '--help  Show this message and exit.' in help_result.output
