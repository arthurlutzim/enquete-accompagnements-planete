"""
Top-level package for
Annuaire des acteurs et actrices de l’accompagnement au numérique libre - Flux.
"""

__author__ = """Arthur Lutz"""
__email__ = 'arthur@lutz.im'
__version__ = '0.1.0'
