"""Console script for enquete_accompagnements_planete."""
import sys
import click
from enquete_accompagnements_planete.enquete_accompagnements_planete import process # noqa


@click.command()
def main(args=None):
    """Console script for enquete_accompagnements_planete."""
    process()
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
