#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('HISTORY.md') as history_file:
    history = history_file.read()

requirements = ['Click>=7.0', 'requests', 'lxml' ]

setup_requirements = ['pytest-runner', ]

test_requirements = ['pytest>=3', ]

setup(
    author="Arthur Lutz",
    author_email='arthur@lutz.im',
    python_requires='>=3.5',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
    ],
    description="Exploration de l'annuaire des acteurs et actrices de l’accompagnement au numérique libre, notamment de leurs flux RSS",
    entry_points={
        'console_scripts': [
            'enquete_accompagnements_planete=enquete_accompagnements_planete.cli:main',
        ],
    },
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='enquete_accompagnements_planete',
    name='enquete_accompagnements_planete',
    packages=find_packages(include=['enquete_accompagnements_planete', 'enquete_accompagnements_planete.*']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://framagit.org/arthurlutz/enquete_accompagnements_planete',
    version='0.1.0',
    zip_safe=False,
)
