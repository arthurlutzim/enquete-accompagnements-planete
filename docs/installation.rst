.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Annuaire des acteurs et actrices de l’accompagnement au numérique libre - Flux, run this command in your terminal:

.. code-block:: console

    $ pip install enquete_accompagnements_planete

This is the preferred method to install Annuaire des acteurs et actrices de l’accompagnement au numérique libre - Flux, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Annuaire des acteurs et actrices de l’accompagnement au numérique libre - Flux can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://framagit.org/arthurlutz/enquete_accompagnements_planete

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://framagit.org/arthurlutz/enquete_accompagnements_planete/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://framagit.org/arthurlutz/enquete_accompagnements_planete
.. _tarball: https://framagit.org/arthurlutz/enquete_accompagnements_planete/tarball/master
